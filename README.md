# Git Flow

## Table of Contents
1. [Introduction](#introduction)
2. [Rules](#rules)
3. [Branches](#branches)
4. [Commits, Merges, Squashes](#commits-merges-squashes)
5. [Git Pull and Rebase](#git-pull-and-rebase)
___
### Introduction
This project is just for the representation of git flows and best practices to handle any git repository
___
### Rules
- There should be one single master branch that shall always be used to create releases
- Each commit before merge shall be rebased first. [See Commits and Merges Section](#Commits-and-Merges)
- Before merging into your main branch (not project's main branch), you must create a PR (Pull Request) and ask for reviews.
- No commits should be merged to the any (main) branch unless it went through the PR (pull request) flow and all the reviews are are resolved
___
### Branches
- A branch should be named according to the purpose of the task. 
    - purpose can be anything, i.e. `feature|hotfix|bugfix` etc
    - a branch name in the end can look something like: `feature/taskName`
___
### Commits Merges Squashes
- If there is an issue available and that issue has a tag-number, then each commit message shall have that tag number as its starting message. 
e.g. `CR-123: Some commit message`
- If there are more than one commits available locally in your branch, then before pushing to origin, it is appreciated if you squash those commits into one.
e.g. 
```bash
git rebase -i HEAD~3
```
- doing this will select the last three commits and open up the git default editor i.e. `vim`
```git
# latest commit
pick commit 1236
pick commit 1235
# first commit from the chosen 3
pick commit 1234
```

```git
pick commit 1236
sqaush commit 1235
squash commit 1234
```
- then save the git file. If you are in vim then just press `esc, wq!, enter`

- Doing so will squash the last three commits into a single commit
- Be advised, it is better to not squash every single commit. If commit has a specific purpose, it is better that it shows up in the git history and should not be squashed. Remember, squashing can change the history.
- After this, just do `git push`.
- Before merging to your main branch, first do a `git push` on your local branch. i.e. if you are on branch `CR-123` and your main branch is `develop`, then after rebasing, do a git push on `CR-123` and then make a PR (pull request) to your main branch.
- Every PR to your main branch should always go through code reviews.
- After PR has been accepted and reviewed, then the branch should be merged to your main branch. 
- After merging, always remember to delete the remote and local branch. i.e. delete `CR-123` from origin and locally.
___
### Git Pull and Rebase
- You should never do a simple `git pull`
    - `git pull` have a tendency to create an unnecessary merge in your checked-out branch
    - It is always better that your pull and rebase. The reason being a linear history. Git is not just to have a backup/footprint of what you did, it should be used to manage your footprint properly and to traverse in past easily. 
    - Linear history makes it easier to rollback at any point without having much issues like overlapping commits etc 
    - Cherry picking is made really easy as well, if you follow rebasing techinque
- To pull any changes from remote or other branch, you should do:
```bash
git checkout <your-main-branch>
git pull --rebase
git checkout <your-local-branch>
git rebase <your-main-branch>
```

- if conflicts then it will go on to interactive rebase mode
    - lets suppose there are 10 commits which came from `<your-main-branch> origin`
    - 4 of those commits have conflicts
    - interactive rebase mode (IRM) will first go to first commit where conflict is
- Resolve that conflict and do:
```bash
git add <file-that-you-resolved> # do this for each resolved file
git rebase --continue
```

- DONOT make a commit for resolved conflict, this will mess up the history. It will also make it difficult to know who implemented the functionality in past and will make cherry picking a living hell
- after successfully going out of IRM you can squash your multiple commits if you like
- To see all the commits you can do something like

```bash
git log --oneline
```